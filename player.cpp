#include <iostream>

#include "player.h"

void player::Print(void) const
{
    std::cout << name << "\t " << score;
}

unsigned int player::GetScore(void) const
{
    return score;
}

void player::SetName(const std::string& nm)
{
    name = nm;
}

void player::SetScore(unsigned int sc)
{
    score = sc;
}
