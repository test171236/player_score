#include <iostream>

#include "player.h"

int main(void)
{
    size_t num = 0;

    std::cout << "Number of players: ";
    std::cin >> num;

    if (num <= 0)
    {
        std::cout << "Incorrect number!\n";
        return 0;
    }

    player* arr = new player[num];

    for (size_t i = 0; i < num; i++)
    {
        std::string s;
        std::cout << "Name of player " << i+1 << ": ";
        std::cin >> s;
        arr[i].SetName(s);

        unsigned int n;
        std::cout << "Score of player " << i+1 << ": ";
        std::cin >> n;
        arr[i].SetScore(n);
    }

    std::qsort(arr, num, sizeof(player), [](const void *a, const void* b)->int {
        unsigned int sc1 = static_cast<const player*>(a)->GetScore();
        unsigned int sc2 = static_cast<const player*>(b)->GetScore();
        if (sc1 > sc2)
            return -1;
        else if (sc1 < sc2)
            return 1;
        return 0;
        });

    std::cout << "\nLeaderboard:\n";
    std::cout << "Place\t Name\t Score\n";
    for (size_t i = 0; i < num; i++)
    {
        std::cout << i+1 << "\t ";
        arr[i].Print();
        std::cout << std::endl;
    }

    delete[num] arr;
    return 0;
}