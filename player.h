#pragma once

#include <string>

class player
{
private:
    std::string name;
    unsigned int score;

public:
    player() : name(""), score(0) {}
    player(const std::string& nm, unsigned int sc) : name(nm), score(sc) {}
    void Print(void) const;
    unsigned int GetScore(void) const;

    void SetName(const std::string& nm);
    void SetScore(unsigned int sc);
};

